<?php

namespace Rss\Controller;

use Rss\Exception\RssEntryNotFoundException;
use Rss\Request\RssEntryRequest;
use Rss\Repository\RssEntryRepositoryInterface;
use Rss\Model\RssEntry;

class RssEntryController extends Controller
{
    private $rssEntryRepository;

    public function __construct(RssEntryRepositoryInterface $rssEntryRepository)
    {
        $this->rssEntryRepository = $rssEntryRepository;
    }

    public function createAction($rssSourceId)
    {
        $rssEntry = new RssEntry();
        $rssEntry->rss_source_id = $rssSourceId;
        return view('Rss::RssEntry/form', [
            'rssEntry' => $rssEntry,
        ]);
    }

    public function editAction($id)
    {
        $rssEntry = $this->rssEntryRepository->find($id);
        if ($rssEntry->id === null) {
            throw new RssEntryNotFoundException($id);
        }
        return view('Rss::RssEntry/form', [
            'rssEntry' => $rssEntry,
        ]);
    }

    public function saveAction(RssEntryRequest $rssEntryRequest, $id = null)
    {
        if ($id === null) {
            $rssEntry = new RssEntry();
        } else {
            $rssEntry = $this->rssEntryRepository->find($id);
        }
        $rssEntry->rss_source_id = $rssEntryRequest->get('rss_source_id');
        $rssEntry->external_id = $rssEntryRequest->get('external_id');
        $rssEntry->title = $rssEntryRequest->get('title');
        $rssEntry->content = $rssEntryRequest->get('content');
        $this->rssEntryRepository->save($rssEntry);
        return response()->json([
            'url' => route('rssIndex', [
                'id' => $rssEntry->rss_source_id,
            ]),
        ]);
    }

    public function deleteAction($id)
    {
        $rssEntry = $this->rssEntryRepository->find($id);
        $this->rssEntryRepository->delete($rssEntry);
        return redirect()->route('rssIndex', ['id' => $rssEntry->rss_source_id]);
    }
}