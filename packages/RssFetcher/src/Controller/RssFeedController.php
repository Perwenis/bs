<?php

namespace Rss\Controller;

use Rss\Request\UrlRequest;
use Rss\Repository\RssEntryRepositoryInterface;
use Rss\Repository\RssSourceRepository;
use Rss\RssFetcher\Entity\RssEntry;
use Rss\RssFetcher\RssFetcherInterface;
use Rss\Model\RssSource;

class RssFeedController extends Controller
{
    private $defaultUrl = 'http://php.net/feed.atom';

    private $rssFetcher;

    private $rssSourceRepository;

    private $rssEntryRepository;

    public function __construct(RssFetcherInterface $fetcher, RssSourceRepository $rssSourceRepository, RssEntryRepositoryInterface $rssEntryRepository)
    {
        $this->rssFetcher = $fetcher;
        $this->rssSourceRepository = $rssSourceRepository;
        $this->rssEntryRepository = $rssEntryRepository;
    }

    public function showAction($id = null)
    {
        if ($id !== null) {
            $rssSource = $this->rssSourceRepository->find($id);
        } else {
            $rssSource = new RssSource();
        }
        if ($rssSource->id === null) {
            $url = $this->defaultUrl;
        } else {
            $url = $rssSource->url;
        }
        return view('Rss::RssFeed/show', [
            'url' => $url,
        ]);
    }

    public function getFeedAction(UrlRequest $request)
    {
        $url = $request->get('url');
        $rssEntries = $this->rssFetcher->fetch($url);
        $rssSource = $this->fetchRssSourceByUrl($url);
        $this->processRssEntries($rssSource, $rssEntries);
        return response()->json([
            'html' => view('Rss::RssFeed/table', [
                'rssEntries' => $rssSource->rssEntries,
                'rssSource' => $rssSource,
            ])->render()
        ]);
    }

    private function fetchRssSourceByUrl(string $url): RssSource
    {
        $rssSource = $this->rssSourceRepository->getByUrl($url);
        if ($rssSource->id === null) {
            $rssSource->url = $url;
            $this->rssSourceRepository->save($rssSource);
        }
        return $rssSource;
    }

    /**
     * @param RssEntry[] $rssEntries
     */
    private function processRssEntries(RssSource $rssSource, array $rssEntries): void
    {
        foreach ($rssEntries as $rssEntry) {
            $rssEntryModel = $this->rssEntryRepository->getByExternalId($rssEntry->getId(), true);
            if ($rssEntryModel->id === null) {
                $rssEntryModel->external_id = $rssEntry->getId();
                $rssEntryModel->title = $rssEntry->getTitle();
                $rssEntryModel->content = $rssEntry->getContent();
                $rssEntryModel->created_at = $rssEntry->getCreatedAt();
                $rssEntryModel->updated_at = $rssEntry->getUpdatedAt();
                $rssEntryModel->RssSource()->associate($rssSource);
                $this->rssEntryRepository->save($rssEntryModel);
            }
        }
    }
}