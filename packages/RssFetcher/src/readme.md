Instalacja już po ściagnięciu paczki composerem (zakładam że Laravel i jego konfiguracja z baza itd. jest zrobiona):
1. Dodać do listy providerów (config/app.php):

> Rss\Provider\RssServiceProvider::class

2. Dodać tabele: 

> php artisan migrate

3. Następnie assets:

> php artisan vendor:publish

dodać je do naszej kompilacji js, np.
> require('../../vendor/rss/js/app');

przekompilować assets:
> npm run dev

<br/>
<b>Znane mi problemy:</b><br/>
1. Jeśli podczas migracji jest błąd 'max key length is 767 bytes' trzeba w jakims providerze dodać:<br/>

> Schema::defaultStringLength(191);

2. Główna ścieżka jest na '/', więc możliwe że trzeba ędzie zakomentować inne routes

3. 