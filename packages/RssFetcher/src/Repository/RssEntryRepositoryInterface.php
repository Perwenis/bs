<?php

namespace Rss\Repository;

use Rss\Model\RssEntry;

interface RssEntryRepositoryInterface
{
    public function save(RssEntry $rssEntry): void;

    public function getByExternalId(string $externalId, bool $withTrashed = false): RssEntry;

    public function find(int $id): RssEntry;

    public function delete(RssEntry $rssEntry): ?bool;
}