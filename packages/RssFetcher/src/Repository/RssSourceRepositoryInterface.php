<?php

namespace Rss\Repository;

use Rss\Model\RssSource;

interface RssSourceRepositoryInterface
{
    public function getByUrl(string $url): RssSource;

    public function save(RssSource $rssSource): void;

    public function find(int $id): RssSource;
}