<?php

namespace Rss\Repository;

use Rss\Model\RssEntry;

class RssEntryRepository implements RssEntryRepositoryInterface
{
    public function save(RssEntry $rssEntry): void
    {
        if ($rssEntry->created_at === null) {
            $rssEntry->created_at = new \DateTime();
        }
        if ($rssEntry->id !== null) {
            $rssEntry->updated_at = new \DateTime();
        }
        $rssEntry->save();
    }

    public function getByExternalId(string $externalId, bool $withTrashed = false): RssEntry
    {
        return $this->getOneByByOneValue('external_id', $externalId, $withTrashed);
    }

    public function find(int $id): RssEntry
    {
        return $this->getOneByByOneValue('id', $id, true);
    }

    private function getOneByByOneValue(string $column, $value, bool $withTrashed): RssEntry
    {
        if ($withTrashed) {
            $rssEntry = RssEntry::withTrashed()->where($column, $value)->first();
        } else {
            $rssEntry = RssEntry::where($column, $value)->first();
        }
        return $rssEntry ?? new RssEntry();
    }

    public function delete(RssEntry $rssEntry): ?bool
    {
        return $rssEntry->delete();
    }
}