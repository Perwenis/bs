<?php

namespace Rss\Repository;

use Rss\Model\RssSource;

class RssSourceRepository implements RssSourceRepositoryInterface
{
    public function getByUrl(string $url): RssSource
    {
        $rssSource = RssSource::where('url', $url)->first();
        return $rssSource ?? new RssSource();
    }

    public function save(RssSource $rssSource): void
    {
        $rssSource->save();
    }

    public function find(int $id): RssSource
    {
        return RssSource::find($id) ?? new RssSource();
    }
}