<?php

Route::get('/{id?}', 'Rss\Controller\RssFeedController@showAction')->name('rssIndex');
Route::post('/getFeed', 'Rss\Controller\RssFeedController@getFeedAction');

Route::get('/rssEntry/create/{rssSourceId}', 'Rss\Controller\RssEntryController@createAction');
Route::post('/rssEntry/save/{id?}', 'Rss\Controller\RssEntryController@saveAction');
Route::get('/rssEntry/edit/{id}', 'Rss\Controller\RssEntryController@editAction');
Route::get('/rssEntry/delete/{id}', 'Rss\Controller\RssEntryController@deleteAction');