$(function() {
    $('#rss-form').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: $(this).prop('method'),
            url: $(this).prop('action'),
            data: $(this).serialize(),
            success: function(data){
                $('#rss-list').html(data.html);
            },
            error: function(XMLHttpRequest) {
                if (XMLHttpRequest.responseJSON.hasOwnProperty('url')) {
                    $('.url-error').html(XMLHttpRequest.responseJSON.url[0]);
                } else {
                    $('.url-error').html('Wystąpił bląd');
                }
            }
        });
    });

    $('#rss-entry-form').submit(function (event) {
        event.preventDefault();
        $('.form-error').remove();
        $.ajax({
            type: $(this).prop('method'),
            url: $(this).prop('action'),
            data: $(this).serialize(),
            success: function(data){
                window.location.href = data.url;
            },
            error: function(XMLHttpRequest) {
                var errors = XMLHttpRequest.responseJSON;
                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        errors[key].forEach(function (error) {
                            $('#' + key).after('<p class="form-error bg-danger">' + error + '</p>')
                        });
                    }
                }
            }
        });
    });
});

