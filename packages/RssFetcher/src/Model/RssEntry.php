<?php

namespace Rss\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RssEntry extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function RssSource()
    {
        return $this->belongsTo(RssSource::class)->withDefault();
    }
}
