<?php

namespace Rss\Model;

use Illuminate\Database\Eloquent\Model;

class RssSource extends Model
{
    public function rssEntries()
    {
        return $this->hasMany(RssEntry::class);
    }
}
