<?php

namespace Rss\Request;

use Illuminate\Foundation\Http\FormRequest;

class UrlRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'url' => 'max:191|url',
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        $messages['url.url'] = 'Podaj poprawny url';
        $messages['url.required'] = 'Url wymgany';
        return $messages;
    }
}
