<?php

namespace Rss\Request;

use Illuminate\Foundation\Http\FormRequest;

class RssEntryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rss_external_id' => 'string|max:191',
            'rss_source_id' => 'required|int|exists:rss_sources,id',
            'title' => 'required|string:max:191',
            'content' => 'required|string',
        ];
    }
}