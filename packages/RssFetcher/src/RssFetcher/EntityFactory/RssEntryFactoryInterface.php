<?php

namespace Rss\RssFetcher\EntityFactory;

use Rss\RssFetcher\Entity\RssEntryInterface;

interface RssEntryFactoryInterface
{
    public function createFromArray(array $data): RssEntryInterface;
}