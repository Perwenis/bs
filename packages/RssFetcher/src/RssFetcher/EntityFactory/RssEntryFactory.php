<?php

namespace Rss\RssFetcher\EntityFactory;

use Rss\RssFetcher\Entity\RssEntry;
use Rss\RssFetcher\Entity\RssEntryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RssEntryFactory implements RssEntryFactoryInterface
{
    public function createFromArray(array $data): RssEntryInterface
    {
        $resolvedData = $this->resolveData($data);
        $rssEntry = new RssEntry();
        $rssEntry->setId($resolvedData['id']);
        $rssEntry->setTitle($resolvedData['title']);
        $rssEntry->setCreatedAt($resolvedData['createdAt']);
        $rssEntry->setUpdateAt($resolvedData['updatedAt']);
        $rssEntry->setContent($resolvedData['content']);
        return $rssEntry;
    }

    private function resolveData(array $data): array
    {
        return $this->getResolver()->resolve($data);
    }

    private function getResolver(): OptionsResolver
    {
        $optionResolver = new OptionsResolver();
        $optionResolver->setDefaults([
            'id' => null,
            'title' => '',
            'createdAt' => null,
            'updatedAt' => null,
            'content' => '',
        ]);
        $optionResolver->setAllowedTypes('id', ['null', 'string']);
        $optionResolver->setAllowedTypes('title', 'string');
        $optionResolver->setAllowedTypes('createdAt', ['null', \DateTime::class]);
        $optionResolver->setAllowedTypes('updatedAt', ['null', \DateTime::class]);
        $optionResolver->setAllowedTypes('content', 'string');
        return $optionResolver;
    }

}