<?php

namespace Rss\RssFetcher\Parser;

use Rss\RssFetcher\Exception\UnsupportedContentFormatException;

class ParserRetriever implements ParserRetrieverInterface
{
    private $availableParsers = [];

    public function __construct(array $parsers)
    {
        $this->availableParsers = $parsers;
    }

    public function getParser(string $type): ParserInterface
    {
        if (!isset($this->availableParsers[$type])) {
            throw new UnsupportedContentFormatException($type);
        }
        return $this->availableParsers[$type];
    }

}