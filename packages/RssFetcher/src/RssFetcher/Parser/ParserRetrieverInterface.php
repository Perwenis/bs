<?php

namespace Rss\RssFetcher\Parser;

interface ParserRetrieverInterface
{
    public function getParser(string $type): ParserInterface;
}