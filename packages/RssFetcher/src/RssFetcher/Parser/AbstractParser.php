<?php

namespace Rss\RssFetcher\Parser;

use Rss\RssFetcher\EntityFactory\RssEntryFactoryInterface;

abstract class AbstractParser implements ParserInterface
{
    protected $rssEntryFactory;

    public function __construct(RssEntryFactoryInterface $rssEntryFactory)
    {
        $this->rssEntryFactory = $rssEntryFactory;
    }
}