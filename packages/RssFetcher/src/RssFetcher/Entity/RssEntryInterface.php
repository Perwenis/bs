<?php

namespace Rss\RssFetcher\Entity;

interface RssEntryInterface
{
    public function getId(): string;

    public function setId(string $id): void;

    public function getTitle(): string;

    public function setTitle(string $title): void;

    public function getCreatedAt(): \DateTime;

    public function setCreatedAt(\DateTime $createdAt): void;

    public function getUpdatedAt(): \DateTime;

    public function setUpdateAt(?\DateTime $updateAt): void;

    public function getContent(): string;

    public function setContent(string $content): void;
}