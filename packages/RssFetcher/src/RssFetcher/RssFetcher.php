<?php

namespace Rss\RssFetcher;

use Rss\RssFetcher\Parser\ParserRetrieverInterface;

class RssFetcher implements RssFetcherInterface
{
    private $rssParserRetriever;

    public function __construct(ParserRetrieverInterface $rssParserRetriever)
    {
        $this->rssParserRetriever = $rssParserRetriever;
    }

    public function fetch(string $url): array
    {
        $data = $this->fetchData($url);
        return $this->parseContent($data['content'], $data['contentType']);
    }

    private function fetchData(string $url): array
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
        $content = curl_exec($curl);
        $contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
        curl_close($curl);
        return [
            'content' => $content,
            'contentType' => $contentType,
        ];
    }

    private function parseContent(string $content, string $type): array
    {
        $parser = $this->rssParserRetriever->getParser($type);
        return $parser->parse($content);
    }
}