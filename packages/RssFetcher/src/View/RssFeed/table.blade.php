<table class="table table-striped table-hover">
    <div class="row">
        <a href="{{ action('\Rss\Controller\RssEntryController@createAction', ['rssSourceId' => $rssSource->id]) }}">Dodaj wpis</a>
    </div>
    <table class="table">
        <caption>Wynik</caption>
        <thead>
        <tr>
            <th>id</th>
            <th>external id</th>
            <th>Tytuł</th>
            <th>Treść</th>
            <th>Edytuj</th>
            <th>Usuń</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($rssEntries as $rssEntry)
            <tr>
                <th scope="row">{{ $rssEntry->id }}</th>
                <td>{{ $rssEntry->external_id }}</td>
                <td>{{ $rssEntry->title }}</td>
                <td>{!! $rssEntry->content !!}</td>
                <td><a href="{{ action('\Rss\Controller\RssEntryController@editAction', ['id' => $rssEntry->id]) }}">Edytuj</a></td>
                <td><a href="{{ action('\Rss\Controller\RssEntryController@deleteAction', ['id' => $rssEntry->id]) }}">Usuń</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</table>